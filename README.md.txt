README MazeSolver by Ruben Heintjens

1) Goal Of the program:

The program is designed to find the path to an exit of a maze that is found in a txt file.
the maze should cosist of " " and "X" where " "=path and "X"= wall.

Example:

XXXXXXX
  X    
X X XXX
X     X
X X X X
X X X X
XXXXXXX

2) How to use:

Place the txt file with your maze in the file of the program. The name of the maze txt file should be maze.txt.

then run the program, start walking by clicking the button.

3) How the program works:

When the program starts a mainFrame is made which contains a buttonpanel and a mazepanel.
The buttonpanel can be used to click the button and show errors.
The mazepanel is used to print the maze.

The mazepanel contains the maze. When it is created it creates a new maze.

The new maze is created by calling a scanner that will read the txt file and create a 2d matrtix of "1" and "0"
In the constructor the scale for drawing is set and the start- and stopfinders are called, 
the setNodes() method sets nodes where needed.
the maze will also be printed to the screen.

Then my goal is to start walking the maze upon clicking the start button.

When Walking the maze:
	1) If the cursor is at a dead end that is not the start the cursor is set to the last seen node.
	2) If not at a dead end:
		if the cursor is at a node it will move in an available direction that is not the direction to return where it came from.
		else the cursor will continue along the path or around the corner.
	3)When at the end the walk method is called. since all directions of the nodes haven been reset if they led to 
	a dead end the cursor should walk along the direct path to the exit while redrawing the path this time only the
	correct route.

4) Improvements

	1) make the program functional
	2) If this would be code for a robod to walk a maze the nodes schould be set while walking since it wouldn't be
	able to scan the maze in advance. Same goes for the EXIT detection.
	3) add more good looking graphics to make it look better.

5)License information
At this point the software can be used by anyone as long as the autor Ruben Heintjens is mentioned in the used/ copied code


