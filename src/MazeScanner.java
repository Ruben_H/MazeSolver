import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author Baptiste Pattyn
 * read an included text-file that contains blank spaces and X-symbols, the blank spaces are blank spaces on the play field and the X-symbols stand for walls
 * the method reads the file and transfers the data into a 2d array that represents the play field
 * this array will be used to navigate and paint the maze
 * you can make your own mazes if you want but keep in mind that the number of cols and rows should be equal
 */
public class MazeScanner {
	private int rows, cols;
	
	/*
	 * method to read the txt file and create the 2d matrix
	 */
	
	public int[][] getMazeArray(){
	String FILENAME = "maze.txt";
	String mazeString = "";
	BufferedReader br = null;
	FileReader fr = null;
	int[][] mazeArray;
	
	try {
		fr = new FileReader(FILENAME);
		br = new BufferedReader(fr);
		
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			cols = sCurrentLine.length();
			rows++;
			mazeString += (sCurrentLine + System.lineSeparator());
		}
	} catch(IOException e){
		e.printStackTrace();
	} 	
	mazeArray = new int[rows][cols];
	Scanner scanner = new Scanner(mazeString); 
	int i = 0;
	while(scanner.hasNextLine()) {
		String line = scanner.nextLine();
		
		for (int j = 0; j < cols; j++) {
			char c = line.charAt(j);
			if((int)c == 'X') {
				mazeArray[i][j] = 1;
			} else {
				mazeArray[i][j] = 0;
			}
			//System.out.print(mazeArray[i][j]);
		}
		//System.out.println("");
		i++;
	}
	scanner.close();
	
	//System.out.println(mazeString);
	for(int i1 = 0; i1 < rows; i1++ ) {
		System.out.println(System.lineSeparator());
		for (int j = 0; j < cols; j++) {
			System.out.print(mazeArray[i1][j]);
		}
	}
	
	return mazeArray;
	}

}
