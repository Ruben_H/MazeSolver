
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * this class creates a panel to control the application
 * all inputs (button ant textFields) and labels are created here
 */

public class ButtonPanel extends JPanel {
	
	private JLabel labelX, labelY, labelWarning, labelGoal, labelStart, labelMaxChildren;
	private JTextField inputX, inputY, inputGoal, inputStart, inputMaxChildren;
	private JButton button;
	/*
	 * constructor for a ButtonPanel
	 */
	public ButtonPanel() {
		Dimension size = getPreferredSize();
		size.width = 350;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createTitledBorder("ControlPanel"));
		
//		this.labelX = new JLabel("Input Multiplier");
//		this.labelY = new JLabel("Input Subtractor");
//		this.labelGoal = new JLabel("What is the end number?");
//		this.labelStart = new JLabel("What is the starting number?");
//		this.labelWarning = new JLabel("");
//		this.labelMaxChildren = new JLabel("children per node");
		
//		this.inputX = new JTextField(10);
//		this.inputY = new JTextField(10);
//		this.inputGoal = new JTextField(10);
//		this.inputStart = new JTextField(10);
//		this.inputMaxChildren = new JTextField(10);
//		this.inputMaxChildren.setText("Don't use, not yet implemented");
		
		this.button = new JButton ("Calculate");
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.weightx = 0.5;
		gc.weighty = 0.5;
		
//		gc.gridx = 0;
//		gc.gridy = 0;
//		add(labelStart, gc);
//		
//		gc.gridx = 1;
//		gc.gridy = 0;
//		add(inputStart, gc);
//		
//		gc.gridx = 0;
//		gc.gridy = 1;
//		add(labelGoal, gc);
//		
//		gc.gridx = 1;
//		gc.gridy = 1;
//		add(inputGoal, gc);
		
//		gc.gridx = 0;
//		gc.gridy = 2;
//		add(labelX, gc);
//		
//		gc.gridx = 1;
//		gc.gridy = 2;
//		add(inputX, gc);
//		
//		gc.gridx = 0;
//		gc.gridy = 3;
//		add(labelY, gc);
//		
//		gc.gridx = 1;
//		gc.gridy = 3;
//		add(inputY, gc);
		
//		gc.gridx = 0;
//		gc.gridy = 4;
//		add(labelMaxChildren, gc);
//		
//		gc.gridx = 1;
//		gc.gridy = 4;
//		add(inputMaxChildren, gc);
//		
		gc.gridx = 0;
		gc.gridy = 5;
		add(button, gc);
//		
//		gc.gridx = 0;
//		gc.gridy = 6;
//		add(labelWarning, gc);
		
		
	}
	/*
	 * getter for the checkButton
	 */
	public JButton getCheckButton() {
		return button;
	}
	/*
	 * getter for the multiplier
	 */
	public JTextField getMultiplier() {
		return inputX;
	}
	/*
	 * getter for the startvalue
	 */
	public JTextField getStart() {
		return inputStart;
	}
	/*
	 * getter for the end goal
	 */
	public JTextField getGoal() {
		return inputGoal;
	}
	/*
	 * getter for the subtractor
	 */
	public JTextField getSubtractor() {
		return inputY;
	}
	/*
	 * getter for the warninglabel used to enable code to set and reset warnings if needed
	 */
	public JLabel getWarning() {
		return labelWarning;
	}
	

}
