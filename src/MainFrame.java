

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/*
 * This class contains the rest of the program and sets a BorderLayout to add items to
 */
public class MainFrame extends JFrame {
	
	private ButtonPanel buttonPanel;
	private JTextArea field;
	private MazePanel maze;
	
	/*
	 * constructor for the MainFrame
	 */
	public MainFrame (String title) {
		super (title);
		
		//set layout
		setLayout(new BorderLayout());
		//create components
		
		buttonPanel = new ButtonPanel();
		field = new JTextArea();
		maze = new MazePanel();
		
		//add components to pane
		Container c = getContentPane();
		
		c.add(buttonPanel, BorderLayout.WEST);
		c.add(maze, BorderLayout.CENTER);
		
		//actions
		
		buttonPanel.getCheckButton().addActionListener(new ActionListener() {
			

			@Override
			/*
			 * (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 * when the button is pressed and valid numbers are given the calculation begins
			 */
			public void actionPerformed(ActionEvent arg0) {
//			Maze mazeNodeSetter = new Maze(this.mazeMatrix);
				
			}
		
		
		});
		

	}
	/*
	 * method to check if a valid integer is inserted into the given JTextField
	 * @param JTextField text
	 */
	
	
}
