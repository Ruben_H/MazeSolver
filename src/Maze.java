import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/*
 * class that defines the maze
 */

public class Maze {
	//richt van waar hij komt blokkeren
	private ArrayList<Node> nodes;
	private String nextNodeName, lastDir;
	private int[][] mazeMatrix;
	private int startX, startY, stopX, stopY, SCALE, a, b, x, y, teller;
	private Node lastSeen,start, stop;
	private boolean found,node;
	
	/*
	 * no argument constructor for a maze
	 */
	public Maze() {
		this.mazeMatrix = this.scan();
		this.nodes = new ArrayList<Node>();
		this.SCALE = 10;
		this.found = false;
		startFinder();
		stopFinder();
		setNodes();
		this.a = startX;
		this.b = startY;
		this.x = startX;
		this.y = startY;
		this.teller = 0;
		this.lastDir = "LEFT";
		this.lastSeen = new Node (startX,startY);
		
	}
	
	/*
	 * method that calls for a scanner to get the maze from the txt file
	 */
	public int[][] scan(){
		MazeScanner mazeScanner = new MazeScanner();
		return mazeScanner.getMazeArray();
	}
	
	/*
	 * paint method to paint the maze ant the cursor
	 */
	public void paint(Graphics g) {
		for (int y=0; y<mazeMatrix.length;y++) {
			for (int x = 0; x<mazeMatrix.length; x++) {
				if (x==0&&y==1) {
					g.setColor(Color.GRAY);
				}
				else {
					
					if (mazeMatrix[y][x] == 0) {
						g.setColor(Color.WHITE);
						
					}
					else {
						g.setColor(Color.BLACK);
						
					}
				}
				g.fillRect(x*SCALE, y*SCALE, SCALE, SCALE);
			}
		}
		g.setColor(Color.RED);
		g.fillRect(a*SCALE, b*SCALE, SCALE, SCALE);
	}
	
	/*
	 * method that creates a node at every location with more tha 2 possible directions (crossroads) and at the start and stop
	 */
	public void setNodes() {
		for (int y=0; y < mazeMatrix.length; y++) {
			for (int x =0; x <mazeMatrix.length; x++) {
				System.out.println("teller "+this.teller);
				if (nodeDetector(x,y)) {
					System.out.println("nodedetector==true");
					Node node = new Node(x,y);
					if (canGoUp(x,y)) {node.setUp(true);}
					if(canGoDown(x,y)) {node.setDown(true);}
					if(canGoRight(x,y)) {node.setRight(true);}
					if(canGoLeft(x,y)) {node.setLeft(true);}
					nodes.add(node);
				}
				else {
					System.out.println("no node was made");
					//go to next, no action needed
				}
			}
		}
		printNodes();
	}
	
	/*
	 * method that walks the maze to find the exit
	 */
	public void walk() {
		
		if(deadEnd()) {
			if (this.x==startX && this.y==startY) {
				this.x=startX+1;
				this.y=startY;
				System.out.println("Im at start, deadend = true, X="+ startX+"y= "+startY);
				
			}
			else {
				System.out.println("Im at a dead end");
				this.x = lastSeen.getX();
				this.y = lastSeen.getY();
				resteDirNode(this.x,this.y);
			}
		}
		else {
			if (nodeDetector(this.x,this.y)) {
				for (Node cursor: nodes) {
					//System.out.println("nodes.size= "+nodes.size());
					System.out.println("this is my ID "+cursor.getID());
					if (cursor.getID() == Integer.toString(this.x)+Integer.toString(this.y)) {
						lastSeen = cursor;
						System.out.println("Let's move "+cursor.getID());
						move(true);
					}
				}
			}
			else {
				System.out.println("im in the else "+nodes.size());
				System.out.println("x "+this.x+"y: "+this.y);
				move(false);
			}
		}
		
		if (this.x== this.stopX && this.y== this.stopY) {
			this.found = true;
		}
		this.a = this.x;
		this.b = this.y;
		walk();
	}
	
	/*
	 * method that detects if the cursor is at a dead end
	 */
	public boolean deadEnd() {
		int xx=this.x;
		int yy=this.y;
		int direction = 0;
		for (int i=0; i<4;i++) {
			if (i==0) {
				if (canGoUp(xx,yy)) {
					direction++;
				}
			}
			if (i==1) {
				if (canGoDown(xx,yy)) {
					direction++;
				}
			}
			if (i==2) {
				if (canGoLeft(xx,yy)) {
					direction++;
				}
			}
			if (i==3) {
				if (canGoRight(xx,yy)) {
					direction++;
				}
			}
		}
		if (direction <= 1) {
			return true;
		}
		else {return false;}
		
		
	}
	
	/*
	 * method that puts already walked directions to false to disable the cursor to re-enter that direction
	 * 
	 * this is called when a dead end was detected to remember where not to go
	 */
	public void resteDirNode(int x, int y) {
		
		if (this.lastSeen.getLastDir() == "UP") {
			for (Node cursor: nodes) {
				if (cursor.getID() == Integer.toString(x)+Integer.toString(y)) {
					cursor.setUp(false);
				}
			}
		}
		if (this.lastSeen.getLastDir() == "DOWN") {
			for (Node cursor: nodes) {
				if (cursor.getID() == Integer.toString(x)+Integer.toString(y)) {
					cursor.setDown(false);
				}
			}
		}
		if (this.lastSeen.getLastDir() == "RIGHT") {
			for (Node cursor: nodes) {
				if (cursor.getID() == Integer.toString(x)+Integer.toString(y)) {
					cursor.setRight(false);
				}
			}
		}
		if (this.lastSeen.getLastDir() == "LEFT") {
			for (Node cursor: nodes) {
				if (cursor.getID() == Integer.toString(x)+Integer.toString(y)) {
					cursor.setLeft(false);
				}
			}
		}
	}
	
	/*
	 * method that executes a step in the maze
	 */
	public void move(boolean node) {
		if (canGoUp(x,y) && this.lastDir != "DOWN") {
			System.out.println("up; this is x: "+this.x+" this is y: "+this.y);
			if(node) {this.lastSeen.setLastDir("UP");}
			this.y-=1;
			this.lastDir = "UP";
			return;
			}
		if(canGoDown(x,y) && this.lastDir != "DOWN") {
			System.out.println("down; this is x: "+this.x+" this is y: "+this.y);
			if(node) {this.lastSeen.setLastDir("DOWN");}
			this.y+=1;
			this.lastDir = "DOWN";
			return;
			}
		if(canGoRight(x,y)&& this.lastDir != "LEFT") {
			System.out.println("right; this is x: "+this.x+" this is y: "+this.y);
			if(node) {this.lastSeen.setLastDir("RIGHT");}
			this.x+=1;
			this.lastDir = "RIGHT";
			return;
			}
		if(canGoLeft(x,y) && this.lastDir != "RIGHT") {
			System.out.println("left; this is x: "+this.x+" this is y: "+this.y);
			if(node) {this.lastSeen.setLastDir("LEFT");}
			this.x-=1;
			this.lastDir = "LEFT";
			return;
			}
	}
	
	//booleans for movement
	
	/*
	 * method that checks if the cursor can move up (true/false)
	 */
	public boolean canGoUp(int x, int y) {
		//System.out.println("up");
		if (y>0 && y<mazeMatrix.length) {
			if (mazeMatrix[y-1][x] == 1) {
			return false;
			}
			else {
				return true;
			}
		}
		else {return false;}
	}
	
	/*
	 * method that checks if the cursor can move down (true/false)
	 */
	public boolean canGoDown(int x, int y) {
		//System.out.println("down");
		if (y<mazeMatrix.length-1) {
			if (mazeMatrix[y+1][x] == 1) {
				return false;
			}
			else {return true;}
		}
		else {return false;}
			
	}
	
	/*
	 * method that checks if the cursor can move left (true/false)
	 */
	public boolean canGoLeft(int x, int y) {
		//System.out.println("left");
		if (x>0) {
			if (mazeMatrix[y][x-1] == 1) {
				return false;
			}
			else {return true;}
		}
		else {return false;}
	}
	
	/*
	 * method that checks if the cursor can move right (true/false)
	 */
	public boolean canGoRight(int x, int y) {
		//System.out.println("right");
		if (x < mazeMatrix.length-1) {
			if (mazeMatrix[y][x+1] == 1) {
				return false;
			}
			else {return true;}
		}
		else {return false;}
	}
	
	//nodeDetection
	
	/*
	 * method that detects is a spot in the maze is a node
	 */
	public boolean nodeDetector(int x, int y) {
		teller++;
		System.out.println("teller: "+teller);
		if (mazeMatrix[y][x] == 0) {
			int direction = 0;
			for (int i=0; i<4;i++) {
				//System.out.println("direction= "+direction);
				if (i==0) {
					if (canGoUp(x,y)) {
						direction++;
					}
				}
				if (i==1) {
					if (canGoDown(x,y)) {
						direction++;
					}
				}
				if (i==2) {
					if (canGoLeft(x,y)) {
						direction++;
					}
				}
				if (i==3) {
					if (canGoRight(x,y)) {
						direction++;
					}
				}
			}
			if (direction >=3) {
				return true;
			}
			else {return false;}
		}
		else {
			//other code bc wall
			return false;
		}
		
	}
	
	//start-end-finders
	
	/*
	 * method that scans the first colum to find the entrance of the maze
	 */
	public void startFinder() {
		for (int i=0;i<mazeMatrix.length;i++) {
			if (mazeMatrix[i][0] == 0) {
				this.startX = 0;
				this.startY = i;
				System.out.println("start found");
				return;
			}
			else {
				System.out.println("no start found");
			}
			
		}
		Node start = new Node(startX, startY);
		nodes.add(start);
		this.start = start;
	}
	
	/*
	 * method that scans the most right colum to find the end of the maze
	 * 
	 * this is not really needed (detection if the cursor is at the biggest possible x index would also mean that the end hes been found)
	 */
	public void stopFinder() {
		for (int i=0;i<mazeMatrix.length;i++) {
			if (mazeMatrix[i][mazeMatrix.length-1] == 0) {
				this.stopX = mazeMatrix.length-1;
				this.stopY = i;
				System.out.println("stop found");
				return;
			}
			else {
				System.out.println("no stop found");
			}
			
		}
		Node stop = new Node(stopX, stopY);
		nodes.add(stop);
		this.stop = stop;
	}
	
	//getters
	
	/*
	 * getter for a node
	 */
	public boolean getNode() {
		return node;
	}
	
	/*
	 * getter for the matrix
	 */
	public  int[][] getMatrix(){
		return mazeMatrix;
	}
	
	/*
	 * getter to known wether the exit has been found yes or no
	 */
	public boolean getFound() {
		return this.found;
	}
	
	/*
	 * getter for the X-coordinate of the start
	 */
	public int getStartX() {
		return this.startX;
	}
	
	
	/*
	 * getter for the Y-coordinate of the start
	 */
	public int getStartY() {
		return this.startY;
	}
	
//	public void getPath() {
//		return path;
//	}
	
	/*
	 * method that prints the ID's of all the created nodes
	 */
	public void printNodes() {
		for (Node cursor:nodes) {
			System.out.println("ID "+cursor.getID());
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * This is a node containing an element used in a singly linked list
	 * 
	 *
	 * 
	 */
	private class Node {
		private int xCor, yCor;
		private Node next, prev;
		private Node[] nodeList;
		private boolean UP,DOWN,LEFT,RIGHT;
		private String id, lastDir;
		
		/**
		 * 
		 * @param x and y
		 */
		public Node(int x, int y) {
			this(x,y, null, null);
		}
		
		/**
		 * @param x and y
		 * @param next
		 * @param prev
		 */
		public Node(int x, int y, Node next, Node prev) {
			this.xCor = x;
			this.yCor = y;
			this.next = next;
			this.prev = prev;
			this.setDirs(false, false, false, false);
			this.id = Integer.toString(x)+Integer.toString(y);
			this.lastDir = null;
			System.out.println("node was made");
			System.out.println("id= "+this.id);

		}
		
		/**
		 * Getter for the x coordinate
		 *  
		 * @return the x coordinate
		 */
		public int getX() {
			return this.xCor;
		}
		
		/**
		 * Getter for the y coordinate
		 *  
		 * @return the y coordinate
		 */
		public int getY() {
			return this.yCor;
		}
		
		/**
		 * Getter for the next node
		 * 
		 * @return next node
		 */
		public Node getNext(){
			return next;
		}
		
		/**
		 * Getter for the next node
		 * 
		 * @return next node
		 */
		public Node getPrev(){
			return prev;
		}
		
		/**
		 * getter for the ID of the node
		 * @return the ID of the node
		 */
		public String getID() {
			return id;
		}
		
		/**
		 * getter for the last direction in which the cursor has traveled over the node
		 * @return the last traveled direction
		 */
		public String getLastDir() {
			return lastDir;
		}
		
		/**
		 * Setter for the next node
		 *  
		 * @param next
		 */
		public void setNext(Node next) {
			this.next = next;
		}
		
		/**
		 * setter for the lastDir variable
		 * @param s
		 */
		public void setLastDir(String s) {
			this.lastDir = s;
		}
		
		/**
		 * setter for the possible directions
		 * @param up
		 * @param down
		 * @param left
		 * @param right
		 */
		public void setDirs(boolean up, boolean down, boolean left, boolean right) {
			setUp(up);
			setDown(down);
			setLeft(left);
			setRight(right);
		}
		
		/**
		 * setter for the up direction
		 * @param up
		 */
		public void setUp(boolean up) {
			this.UP = up;
		}
		
		/**
		 * setter for the down direction
		 * @param down
		 */
		public void setDown (boolean down) {
			this.DOWN = down;
		}
		
		/**
		 * setter for the left direction
		 * @param left
		 */
		public void setLeft(boolean left) {
			this.LEFT = left;
		}
		
		/**
		 * setter for the right direction
		 * @param right
		 */
		public void setRight(boolean right) {
			this.RIGHT = right;
		}

	}

}
