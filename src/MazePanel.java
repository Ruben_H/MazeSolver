import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/*
 * Class that fills the part of the screen that has to do with the maze itself
 */
public class MazePanel extends JPanel {

	@SuppressWarnings("serial")


	public static final Color GREEN = new Color(1666073);
	private int[][] mazeMatrix;
	private Maze maze;
	private boolean running;
	
	/*
	 * constructor for the MazePanel
	 * 
	 * creates a new maze() and gets the program going
	 * if run is disabled the program will print the maze (run does not function and is therefore disabled)
	 */
	public MazePanel() {
		this.maze = new Maze();
		running = true;
		//run();
	}
	
	
	/*
	 * method that sets the nodes, calls repaint() and stops te program from running when the exit has been found
	 */
	public void run() {
		
		maze.setNodes();
		
		while (running) {
			repaint();
			if (maze.getFound()) {
				running = false;
			}
			else {
				System.out.println("ik start hier: "+maze.getStartX()+", y= "+ maze.getStartY());
				maze.walk();
			}
			
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 * 
	 * calls the paint(g) method from the maze
	 */
	@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			//mazeMatrix = MazeNodeSetter.getMatrix();
			
			maze.paint(g);
			
			
			
			
		}
	}


